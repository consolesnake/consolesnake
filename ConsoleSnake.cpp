﻿#include "pch.h"
#include <iostream>
#include <Windows.h>
#include <conio.h>

using namespace std;

bool gameOver;
const int width = 20;
const int height = 20;
int x, y, fruitX, fruitY, score;
enum eDirection { STOP = 0, LEFT, RIGHT, UP, DOWN };
eDirection dir;
struct Block {
	int x, y;
};


Block snake[25];
int snake_llen;


void Setup() {
	gameOver = false;
	dir = RIGHT;
	x = width / 2;
	y = height / 2;
	fruitX = (rand() % (width - 2)) + 1;
	fruitY = (rand() % (height - 2)) + 1;
	score = 0;
	snake_llen = 3;

	snake[0].x = 4;
	snake[0].y = 3;

	snake[1].x = 3;
	snake[1].y = 3;

	snake[2].x = 2;
	snake[2].y = 3;

}

void Draw() {
	Sleep(100);
	COORD pos;
	for (int i = 0; i < snake_llen; i++) {
		pos.X = snake[i].x;
		pos.Y = snake[i].y;
		SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
		cout << '0';
	}
	pos.X = fruitX;
	pos.Y = fruitY;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
	cout << 'f';
	pos.X = 0;
	pos.Y = height+1;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
	cout << "score: " << snake_llen <<" - 1" ;
	/*for (int i = 0; i < width; i++)
		cout << "#";
	cout << endl;

	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			bool flag = false;
			for (int k = 0; k < snake_llen; k++) {
				if (snake[k].y == i && snake[k].x == j) {
					flag = true;
					break;
				}
			}
			if (j == 0 || j == width - 1)
				cout << "#";
			if (flag == true)
				cout << "0";
			else if (i == fruitY && j == fruitX)
				cout << "F";
			else
				cout << " ";
		}
		cout << endl;
	}
	for (int i = 0; i < width; i++)
		cout << "#";
	cout << endl;*/
}

void Input() {
	if (_kbhit()) {
		switch (_getch_nolock())
		{
		case 'a':
			if (dir != RIGHT)
				dir = LEFT;
			break;
		case 'd':
			if (dir != LEFT)
				dir = RIGHT;
			break;
		case 'w':
			if (dir != DOWN)
				dir = UP;
			break;
		case 's':
			if (dir != UP)
				dir = DOWN;
			break;
		}
	}

}

void drawfield() {
	system("cls");
	for (int i = 0; i < width; i++)
		cout << "#";
	cout << endl;

	for (int i = 0; i < height -2 ; i++) {
		for (int j = 0; j < width; j++) {
			
			
			if (j == 0 || j == width - 1)
				cout << "#";
			
			
			else
				cout << " ";
		}
		cout << endl;
	}
	for (int i = 0; i < width; i++)
		cout << "#";
	cout << endl;
}

void Logic() {
	int oldx = snake[snake_llen - 1].x; int oldy = snake[snake_llen - 1].y;
	COORD pos;
	pos.X = oldx;
	pos.Y = oldy;
	SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
	cout << ' ';
	for (int i = snake_llen - 1; i > 0; i--) {
		snake[i].x = snake[i - 1].x;
		snake[i].y = snake[i - 1].y;
	}
	int dx = 0, dy = 0;
	switch (dir) {
	case LEFT:dx = -1; break;
	case RIGHT:dx = 1; break;
	case UP:dy = -1; break;
	case DOWN:dy = 1; break;
	}
	snake[0].x += dx;
	snake[0].y += dy;
	
		if (snake[0].x == fruitX && snake[0].y == fruitY) {
			snake_llen++;
			fruitX = ( rand() % (width-2) )+1;
			fruitY = ( rand() % (height-2) )+1;
		}
		if (snake[0].x == width-1) {
			snake[0].x = 1;
		}
		else if (snake[0].x < 1) {
			snake[0].x = width - 2;
		}
		if (snake[0].y == height -1) {
			snake[0].y = 1;
		}
		else if (snake[0].y < 1) {
			snake[0].y = height - 2;
		}

		for (int i = 1; i < snake_llen; i++) {
			if (snake[0].x == snake[i].x && snake[0].y == snake[i].y) {
				drawfield();
				pos.X= 5;
				pos.Y= 10;
				SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
				cout << "Game Over!";
				pos.X = 4;
				pos.Y = 11;
				SetConsoleCursorPosition(GetStdHandle(STD_OUTPUT_HANDLE), pos);
				cout << "Press Enter";
				getchar();
				Setup(); 
				drawfield();
			}
		}

		if (snake_llen == 25) {
			Setup();
			drawfield();
		}


			
}

int main() {
	Setup();
	drawfield();
	while (!gameOver) {
		Draw();
		Input();
		Logic();
	}
	return 0;
	
}